﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoseCollider : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        /* Scene_Loader scene= new Scene_Loader();
         scene.LoadNextScene();*/
        SceneManager.LoadScene("GameOver");
    }

}

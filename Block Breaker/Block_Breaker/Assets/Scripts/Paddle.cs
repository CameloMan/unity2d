﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{
    [SerializeField] float screenWidthInUnits;
    [SerializeField] float min;
    [SerializeField] float max;

    //cached references
    GameSession myGameSession;
    Ball myBall;

    //I use the start method to initialization of the refernces and variables
    private void Start()
    {
        myGameSession = FindObjectOfType<GameSession>();
        myBall = FindObjectOfType<Ball>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 paddlePos = new Vector2(transform.position.x, transform.position.y);
        paddlePos.x = Mathf.Clamp(GetXPos(), min, max);
        transform.position = paddlePos;
    }

    private float GetXPos()
    {
        if (myGameSession.IsAutoPlayEnabled())
        {
            return myBall.transform.position.x;

        }
        else
        {
            return Input.mousePosition.x / Screen.width * screenWidthInUnits; ;
        }
    }
}

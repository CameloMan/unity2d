﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [SerializeField] private int breakableBlocks;
    Scene_Loader scene;
   
    private void Start()
    {
        scene = FindObjectOfType<Scene_Loader>();
    }
    public int BreakableBlocks { get => breakableBlocks; set => breakableBlocks = value;  }

    public void BlockDestroyed()
    {
       
        BreakableBlocks--;
        if (breakableBlocks == 0)
        {
            scene.LoadNextScene();
        }
    }

    public void CountBreakableBlocks()
    {
        BreakableBlocks++;
    }
}

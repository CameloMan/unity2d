﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AttackerSpawner : MonoBehaviour
{
    bool spawn = true;
    [SerializeField] GameObject AttackerPrefab;
    [SerializeField] float minTimeBetweenSpawns = 1;
    [SerializeField] float maxTimeBetweenSpawns = 5;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        while (spawn)
        {
            yield return new WaitForSeconds(Random.Range(minTimeBetweenSpawns, maxTimeBetweenSpawns));
            SpawnEnemies();
        }
    }

    private void SpawnEnemies()
    {
        Instantiate(AttackerPrefab, transform.position, transform.rotation);
    }

    // Update is called once per frame
    void Update()
    {

    }
}

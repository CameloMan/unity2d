﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] List<WaveConfig> waveConfigs;
    [SerializeField] int startingWave = 0;
    [SerializeField] bool loopping = false;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        do
        {
            yield return StartCoroutine(SpawnAllWaves());
        } while (loopping);
    }

    private IEnumerator SpawnAllWaves()
    {
        for (int waveIndex = startingWave; waveIndex < waveConfigs.Count; waveIndex++)
        {
            var currentWave = waveConfigs[waveIndex];
            yield return StartCoroutine(SpawnAllEnemiesInWave(currentWave));

        }
    }

    private IEnumerator SpawnAllEnemiesInWave(WaveConfig waveConfig)
    {
        for (int i = 1; i <= waveConfig.NumberOfEnemies; i++)
        {
            var newEnemy = Instantiate(waveConfig.EnemyPrefab,
                                    waveConfig.GetWaypoints()[0].transform.position,
                                    waveConfig.EnemyPrefab.transform.rotation);
            newEnemy.GetComponent<EnemyPathing>().WaveConfig = waveConfig;
            yield return new WaitForSeconds(waveConfig.TimeBetweenSpawns);
        }
        //  finishWave = true;
        // startingWave++;
    }

    // Update is called once per frame
    void Update()
    {
        /* if (finishWave && (startingWave < waveConfigs.Count))
         {
             finishWave = false;
             var currentWave = waveConfigs[startingWave];
             StartCoroutine(SpawnAllEnemiesInWave(currentWave));
         }*/
    }
}

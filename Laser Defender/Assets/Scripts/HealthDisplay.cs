﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HealthDisplay : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI healthText;
    Player player;
    void Start()
    {
        player = FindObjectOfType<Player>();
        healthText.text = player.Health.ToString();
    }

    void Update()
    {
        healthText.text = player.Health.ToString();
    }
}

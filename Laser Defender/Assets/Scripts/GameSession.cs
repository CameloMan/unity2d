﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSession : MonoBehaviour
{
    [Range(0.1f, 10f)] [SerializeField] float gemeSpeed = 1f;

    [SerializeField] int currentScore = 0;

    public int CurrentScore { get => currentScore; set => currentScore = value; }

    private void Awake()
    {
        int gameStatusCount = FindObjectsOfType<GameSession>().Length;

        if (gameStatusCount > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }
   

    // Update is called once per frame
    void Update()
    {
        Time.timeScale = gemeSpeed;
    }
    public void AddToScore(int pointsPerBlockDestroyed)
    {
        CurrentScore += pointsPerBlockDestroyed;
    }
    public void ResetGame()
    {
        Destroy(gameObject);
    }
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] GameObject projectilePreFab;
    [SerializeField] GameObject destructionPreFab;
    [SerializeField] float projectileSpeed = 10f;
    [SerializeField] float minTimeDestruction = 0.2f;
    [SerializeField] float maxTimeDestruction = 3f;
    float timeDestruction;
    // Start is called before the first frame update
    void Start()
    {
        timeDestruction = Random.Range(minTimeDestruction, maxTimeDestruction);
    }

    // Update is called once per frame
    void Update()
    {
        timeDestruction -= Time.deltaTime;
        if (timeDestruction <= 0f)
        {
            DestructionSecuence();
            ExplosionProjectiles();
        }
    }

    private void ExplosionProjectiles()
    {
        for (int i = 0; i < 360; i += 20)
        {
            Fire(i);
        }
    }

    private void DestructionSecuence()
    {
        Destroy(gameObject);
        var destruction = Instantiate(destructionPreFab, transform.position, Quaternion.identity);
        destruction.transform.localScale = transform.localScale;
        Destroy(destruction, 1);
    }

    private void Fire(int degrees)
    {
        Vector3 LaserToShip = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        GameObject projectile = Instantiate(projectilePreFab, LaserToShip, Quaternion.Euler(Vector3.forward * (degrees - 90)));
        float velx = projectileSpeed * Mathf.Cos(degrees * Mathf.Deg2Rad);
        float vely = projectileSpeed * Mathf.Sin(degrees * Mathf.Deg2Rad);
        projectile.GetComponent<Rigidbody2D>().velocity = new Vector2(velx, vely);
        // projectile.GetComponent<Rigidbody2D>().velocity = transform.TransformDirection(new Vector3(0,1,0) * projectileSpeed);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DestructionSecuence();
    }
}

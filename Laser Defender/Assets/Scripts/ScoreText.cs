﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class ScoreText : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI healthText;
    GameSession session;

    void Start()
    {
        session = FindObjectOfType<GameSession>();
    }

    // Update is called once per frame
    void Update()
    {
        healthText.text = session.CurrentScore.ToString();
    }
}

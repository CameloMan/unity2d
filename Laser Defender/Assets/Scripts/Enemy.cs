﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    [Header("Enemy Stats")]
    [SerializeField] float health = 100;
    [SerializeField] GameObject detructionAnimation;
    [SerializeField] GameObject detructionParticle;
    [Header("Shooting")]
    [SerializeField] float projectileSpeed = 10f;
    float shotCounter;
    [SerializeField] float minTimeBetweenShots = 0.2f;
    [SerializeField] float maxTimeBetweenShots = 3f;
    [SerializeField] int pointsDestroyed;
    [SerializeField] GameObject projectilePrefab;
    [Header("Sounds")]
    [SerializeField] AudioClip destroySound;
    [SerializeField] [Range(0, 1)] float volume = 0.1f;
    [SerializeField] AudioClip shootSound;
    // Start is called before the first frame update
    void Start()
    {
        shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
        //detructionAnimation.transform.localScale = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        CountDownAndShoot();
    }

    private void CountDownAndShoot()
    {
        shotCounter -= Time.deltaTime;
        if (shotCounter <= 0f)
        {
            Fire();
            shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
        }
    }

    private void Fire()
    {
        AudioSource.PlayClipAtPoint(shootSound, Camera.main.transform.position, volume);
        Vector3 LaserToShip = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
        GameObject projectile = Instantiate(projectilePrefab, LaserToShip, Quaternion.identity);
        projectile.GetComponent<Rigidbody2D>().velocity = transform.TransformDirection(Vector3.up * projectileSpeed);
    }



    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damegeDealer = other.gameObject.GetComponent<DamageDealer>();
        if (!damegeDealer) { return; }
        procesHit(damegeDealer);
    }

    private void procesHit(DamageDealer damegeDealer)
    {
        if (health > damegeDealer.Damage)
        {
            damegeDealer.Hit();
            health -= damegeDealer.Damage;
        }
        else
        {
            DestroyEnemy();
        }
    }

    private void DestroyEnemy()
    {
        FindObjectOfType<GameSession>().AddToScore(pointsDestroyed);
        Die(detructionAnimation);
        Die(detructionParticle);
        AudioSource.PlayClipAtPoint(destroySound, Camera.main.transform.position, volume);
        Destroy(gameObject);
    }

    private void Die(GameObject detructionVar)
    {
        var destruction = Instantiate(detructionVar, transform.position, Quaternion.identity);
        if (detructionVar.tag != "Enemy")
        {
            destruction.transform.localScale = transform.localScale;
        }
        Destroy(destruction, 1f);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    //Configuration parameters
    [Header("Player")]
    [SerializeField] float moveSpeed = 10f;
    [SerializeField] float padding = 1f;
    [SerializeField] int health = 1000;
    [SerializeField] GameObject detructionAnimation;
    [SerializeField] AudioClip destroySound;
    [SerializeField] AudioClip shootSound;
    [SerializeField] AudioClip hitSound;
    [SerializeField] [Range(0, 1)] float volume = 0.1f;
    [SerializeField] string GameOverScene;
    [SerializeField] float TimeBeforeGameOver;
    [SerializeField] Scene_Loader SceneM;
    [Header("Projectile")]
    [SerializeField] GameObject laserPrefab;
    [SerializeField] float projectileSpeed = 10f;
    [SerializeField] float projectileFiringPeriod = 0.5f;
    [SerializeField] GameObject laserBlastPrefab;

    Coroutine firingCoroutine;
    bool AutoFiring = true;
    GameObject Blast;
    float xMin;
    float xMax;
    float yMin;
    float yMax;

    public int Health { get => health; set => health = value; }

    void Start()
    {
        SetUpMoveBoundaries();
    }
    // Update is called once per frame
    void Update()
    {
        Move();
        Fire();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DamageDealer damegeDealer = other.gameObject.GetComponent<DamageDealer>();
        if (!damegeDealer) { return; }
        procesHit(damegeDealer);
    }

    private void procesHit(DamageDealer damegeDealer)
    {
        if (Health > damegeDealer.Damage)
        {
            AudioSource.PlayClipAtPoint(hitSound, Camera.main.transform.position, volume);
            damegeDealer.Hit();
            Health -= damegeDealer.Damage;
        }
        else
        {
            AudioSource.PlayClipAtPoint(destroySound, Camera.main.transform.position, volume);
            var destruction = Instantiate(detructionAnimation, transform.position, Quaternion.identity);
            destruction.transform.localScale = transform.localScale;
            Destroy(Blast);
            Destroy(destruction, 1f);
            StartCoroutine(GameOver());
        }
    }

    IEnumerator GameOver()
    {
        gameObject.GetComponent<Collider2D>().enabled = false;
        gameObject.GetComponent<Renderer>().enabled = false;
        yield return new WaitForSeconds(TimeBeforeGameOver);
        SceneM.LoadScene(GameOverScene);
        Destroy(gameObject);
    }

    private void Move()
    {
        var deltaX = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        var deltaY = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;
        var newXPos = Mathf.Clamp(transform.position.x + deltaX, xMin, xMax);
        var newYPos = Mathf.Clamp(transform.position.y + deltaY, yMin, yMax);
        transform.position = new Vector3(newXPos, newYPos, transform.position.z);
    }

    private void SetUpMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + padding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - padding;
        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + padding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - padding;
    }
    private void Fire()
    {

        if (Input.GetButtonDown("Fire1"))
        {
            if (Blast != null)
            {
                Destroy(Blast);
            }
            GenerateBlast();
            AutoFiring = true;
            firingCoroutine = StartCoroutine(FireHold());
        }
        else if (Input.GetButtonUp("Fire1"))
        {
            StopCoroutine(firingCoroutine);
            AutoFiring = false;
            Destroy(Blast);

        }
        if (Blast != null)
        {
            Blast.transform.position = new Vector3(transform.position.x, transform.position.y + 0.8f, Blast.transform.position.z);
        }

    }

    IEnumerator FireHold()
    {
        if (!AutoFiring)
        {
            Destroy(Blast);
        }
        while (AutoFiring)
        {
            AudioSource.PlayClipAtPoint(shootSound, Camera.main.transform.position, volume);
            FireLaser();

            yield return new WaitForSeconds(projectileFiringPeriod);
        }

    }

    private void GenerateBlast()
    {
        Vector3 BlastToShip = new Vector3(transform.position.x, transform.position.y + 0.8f, transform.position.z);
        Blast = Instantiate(laserBlastPrefab, BlastToShip, laserBlastPrefab.transform.rotation);
    }

    private void FireLaser()
    {
        Vector3 LaserToShip = new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z);
        GameObject laser = Instantiate(laserPrefab, LaserToShip, Quaternion.identity);
        laser.GetComponent<Rigidbody2D>().velocity = transform.TransformDirection(Vector3.up * projectileSpeed);
        //Destroy(laser, 1.1f);
    }


}

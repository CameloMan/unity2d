﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene_Loader : MonoBehaviour
{

    public void LoadNextScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }
    public void LoadScene(string Scene)
    {
        SceneManager.LoadScene(Scene);
    }
    public void LoadStartcene()
    {
        FindObjectOfType<GameSession>().ResetGame();
        SceneManager.LoadScene("Level 1");
    }
    public void LoadMenuScene()
    {
        FindObjectOfType<GameSession>().ResetGame();
        SceneManager.LoadScene("Start Game");
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

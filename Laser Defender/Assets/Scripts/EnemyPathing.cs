﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPathing : MonoBehaviour
{
    WaveConfig waveConfig;
    List<Transform> waypoints;
    int waypointIndex = 0;

    public WaveConfig WaveConfig { get => waveConfig; set => waveConfig = value; }

    void Start()
    {
        waypoints = WaveConfig.GetWaypoints();
        transform.position = waypoints[waypointIndex].transform.position;
    }
    void Update()
    {
        MoveMethod();
    }

    private void MoveMethod()
    {
        if (waypointIndex <= waypoints.Count - 1)
        {
            var step = WaveConfig.MoveSpeed * Time.deltaTime;

            var targetPosition = waypoints[waypointIndex].transform.position;
            transform.position = Vector2.MoveTowards(transform.position, targetPosition, step);
            if (transform.position == targetPosition)
            {
                waypointIndex++;
            }
        }
        else
        {
            Destroy(gameObject);
        }
    }
}

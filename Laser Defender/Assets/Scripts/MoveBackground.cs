﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MoveBackground : MonoBehaviour
{
    [SerializeField] float padding = 1f;
    [SerializeField] float secondsToCount = 1;
    [SerializeField] float RangeOfMovement = 1;

    float secondsCounter = 0;


    float xMin;
    float xMax;
    float yMin;
    float yMax;

    void Start()
    {
        SetUpMoveBoundaries();
    }



    // Update is called once per frame
    void Update()
    {
        secondsCounter += Time.deltaTime;
        if (secondsCounter >= secondsToCount)
        {
            secondsCounter = 0;
            Move();
        }

    }

    private void Move()
    {

        float deltaY;
        if (transform.position.y < (yMin * 2))
        {
            deltaY = yMax;
        }
        else
        {
            deltaY = transform.position.y - 0.1f;
        }

        var deltaX = Random.Range(-RangeOfMovement, RangeOfMovement);
        // var deltaY = Random.Range(-RangeOfMovement, RangeOfMovement);
        var newXPos = Mathf.Clamp(transform.position.x + deltaX, xMin, xMax);
        // var newYPos = Mathf.Clamp(transform.position.y + deltaY, yMin, yMax);
        transform.position = new Vector3(newXPos, deltaY, transform.position.z);//The 2 in the Z is for a bug that I dont found

    }

    private void SetUpMoveBoundaries()
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + padding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - padding;
        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y ;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y + 5;

    }
}

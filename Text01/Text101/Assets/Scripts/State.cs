﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "State")]
public class State : ScriptableObject
{
    [SerializeField] string titleText;
    [TextArea(10, 14)] [SerializeField] string storyText;
    [SerializeField] State[] nextStates;
    public string GetStateStory()
    {
        return storyText;
    }
    public string GetStateTitle()
    {
        return titleText;
    } 
    public State[] GetNextStates()
    {
        return nextStates;
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AdventureGame : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI titleComponent;
    [SerializeField] Text textComponent;//Means that we have this aviable in the inspector 
    [SerializeField] State startingState;

    //string[] daysofTheWeek = { "Monday", "Tuesday", "Wendsday", "Thursday","Friday","Saturaday","Sunday" };//Exanple array

    State state;
    void Start()
    {
        state = startingState;
        titleComponent.text = state.GetStateTitle();
        textComponent.text = state.GetStateStory();
        //  Debug.Log(daysofTheWeek[1]);  //Calling array position  
    }

    // Update is called once per frame
    void Update()
    {
        ManageState();
    }

    private void ManageState()
    {
        var nextStates = state.GetNextStates();
        for (int index = 0; index < nextStates.Length; index++)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1 + index))//the 'keycode' is a integer and we can add values to it
            {
                state = nextStates[index];
            }
        }

        if (Input.GetKeyUp(KeyCode.Q))
        {
            state = startingState;
        }
        titleComponent.text = state.GetStateTitle();
        textComponent.text = state.GetStateStory();
    }
}

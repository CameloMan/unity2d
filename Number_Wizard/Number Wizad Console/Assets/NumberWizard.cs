﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberWizard : MonoBehaviour
{
    static int max;
    static int min;
    int guess;
    // Start is called before the first frame update
    void Start()
    {
        StartGame();

    }
    void StartGame()
    {
        max = 1000;
        min = 1;
        guess = (min + max) / 2;
        Debug.Log("Welcome to number wizard, yo");
        Debug.Log("Pick a number");
        Debug.Log("Highest number is: " + max);
        Debug.Log("Lowest number is: " + min);
        Debug.Log("Tell me if your number is higher or lower than " + guess);
        Debug.Log("Push Up = higher, Push Down = Lower, Push enter = Correct");
        max++;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            //Debug.Log("Up key was pressed.");
            min = guess;
            NextGuess();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            //Debug.Log("Down key was pressed.");
            max = guess;
            NextGuess();
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("Correct number.");
            StartGame();
        }
        /*if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            Debug.Log("Space key was released.");
        }*///If we want to know when the player realize the button

    }

    void NextGuess()
    {
        guess = (max + min) / 2;
        Debug.Log("Tell me if your number is higher or lower than " + guess);

    }
}

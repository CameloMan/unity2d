﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NumberWizard : MonoBehaviour
{
    [SerializeField] int max;
    [SerializeField] int min;
    [SerializeField] TextMeshProUGUI guessText;
    int guess;

    void Start()
    {
        StartGame();
    }
    void StartGame()
    {
        //max--;
        //min--;
        NextGuess();
        //max++;
    }
    public void ChangeBox(string HigLow)
    {
        //min = (HigLow=="Higher")?guess: min;
        if (HigLow == "Higher")
        {
            min = (min == max) ? min : guess + 1;
            // NextGuess();
            //min++;
            //max++; 
        }
        if (HigLow == "Lower")
        {
            max = (min == max || min > max) ? min : guess - 1;
            // NextGuess();
            //max--;
            //min++;
        }
        // max = (HigLow=="Lower")?guess: max;
        NextGuess();
    }
    void NextGuess()
    {
        guess = Random.Range(min, max + 1);
        guessText.text = guess.ToString();
    }
}
